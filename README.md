# Tensorflow/Tensorboard container demo #

This demonstrates running two containers, one with Tensorflow with Jupyter 
notebook support and the other with Tensorboard monitoring the events from the 
first container.

The only prerequisite for this _should_ be having Docker installed.

To run the demo:

1. Clone this repository: ```git clone git@bitbucket.org:rherrick/tf-demo.git```
1. Cd into the repo folder: ```cd tf-demo```
1. Run the launch script: ```./launch-tb.sh```

Once the script has completed, you should see a message like this:

```bash
$ ./launch-tb.sh
20feb699c3a5cb112368d84228a688fafcd28f1e368b804c20d5fa2edfdf6bc9
5b3c0513318c8f4d8ab2397ee3a25e025cbb67bc2b8a9ce28b22ae5ef4391ea4

You can log into the Jupyter notebook at http://localhost:8888 with the token: d402fc4e847f00059369adc9204a2021ab673f8adf08d815

You can view Tensorboard at http://localhost:6006
```

Open the [first URL](http://localhost:8888) and you'll see the notebook browser
with two folders, **notebooks** and **tensorflow-tutorials**. Click into the
**notebooks** folder, then **classification_with_tensorboard.ipynb**.

Open the [second URL](http://localhost:6006) in another browser tab or window.
Unlike the notebook, you don't need a token to log into Tensorboard. You
won't see anything here the first time because there are no events to display.

Go back to the classification notebook and run the code, either by clicking
**Run** to go through each step (which is interesting in and of itself) or by
clicking the double-right arrow icon or the **Kernel -> Restart & Clear Output**
menu command to run the entire notebook at once.

Once you've completed that, go back to the Tensorboard tab. You may need to 
reload the page, but you should now see some (relatively uninteresting)
information about the model you just trained.
