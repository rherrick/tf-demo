#!/usr/bin/env bash

[[ ${0} =~ ^\./launch-tb.sh$ ]] || { echo "You must run this script from the folder that contains it."; exit 1; }

getList() {
    docker exec --interactive --tty tensorflow jupyter notebook list
}

getToken() {
    getList | fgrep http | cut -f 2 -d = | cut -f 1 -d " "
}

countOutputLines() {
    getList | wc -l
}

NB_PATH=$(pwd)/notebooks
[[ -d ${NB_PATH}/logs && $(ls ${NB_PATH}/logs | wc -l) -gt 0 ]] && { mkdir -p ${NB_PATH}/archives; mv ${NB_PATH}/logs ${NB_PATH}/archives/logs-$(date "+%Y%m%d%H%M%S"); }

docker run --interactive --detach --rm --tty --publish 8888:8888 --volume ${NB_PATH}:/tf/notebooks --name=tensorflow tensorflow/tensorflow:latest-py3-jupyter
docker run --interactive --detach --rm --tty --publish 6006:6006 --volume ${NB_PATH}:/tf/notebooks --name=tensorboard tensorflow/tensorflow:latest-py3 tensorboard --logdir /tf/notebooks/logs --host 0.0.0.0

while [[ $(countOutputLines) -lt 2 ]]; do
    sleep 1
done

JUPYTER_TOKEN="$(getToken)"

echo
echo "You can log into the Jupyter notebook at http://localhost:8888 with the token: ${JUPYTER_TOKEN}"
echo
echo "You can view Tensorboard at http://localhost:6006"
echo
